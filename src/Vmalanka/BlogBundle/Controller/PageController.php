<?php

namespace Vmalanka\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

// Import new namespaces
use Symfony\Component\HttpFoundation\Request;
use Vmalanka\BlogBundle\Entity\Enquiry;
use Vmalanka\BlogBundle\Form\EnquiryType;

class PageController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()
                   ->getManager();

        $blogs = $em->getRepository('VmalankaBlogBundle:Blog')
                    ->getLatestBlogs();

        return $this->render('VmalankaBlogBundle:Page:index.html.twig', array(
            'blogs' => $blogs
        ));
    }

    public function aboutAction()
    {
        return $this->render('VmalankaBlogBundle:Page:about.html.twig');
    }

    public function contactAction(Request $request)
    {
        $enquiry = new Enquiry();

        $form = $this->createForm(EnquiryType::class, $enquiry);

        if ($request->isMethod($request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $message = \Swift_Message::newInstance()
                    ->setSubject('Contact enquiry from symblog')
                    ->setFrom('enquiries@blog')
                    ->setTo($this->container->getParameter('vmalanka_blog.emails.contact_email'))
                    ->setBody($this->renderView('VmalankaBlogBundle:Page:contactEmail.txt.twig', array('enquiry' => $enquiry)));

                $this->get('mailer')->send($message);

                $this->get('session')->getFlashBag()->add('blogger-notice', 'Your contact enquiry was successfully sent. Thank you!');

                // Redirect - This is important to prevent users re-posting
                // the form if they refresh the page
                return $this->redirect($this->generateUrl('VmalankaBlogBundle_contact'));
            }
        }

        return $this->render('VmalankaBlogBundle:Page:contact.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function sidebarAction()
    {
        $em = $this->getDoctrine()
                   ->getManager();

        $tags = $em->getRepository('VmalankaBlogBundle:Blog')
                   ->getTags();

        $tagWeights = $em->getRepository('VmalankaBlogBundle:Blog')
                         ->getTagWeights($tags);

        $commentLimit   = $this->container
                           ->getParameter('vmalanka_blog.comments.latest_comment_limit');
        $latestComments = $em->getRepository('VmalankaBlogBundle:Comment')
                             ->getLatestComments($commentLimit);

        return $this->render('VmalankaBlogBundle:Page:sidebar.html.twig', array(
            'latestComments'    => $latestComments,
            'tags'              => $tagWeights
        ));
    }
}
